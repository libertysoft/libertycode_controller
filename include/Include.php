<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/DefaultMethodInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/controller/library/ConstController.php');
include($strRootPath . '/src/controller/library/ToolBoxController.php');
include($strRootPath . '/src/controller/exception/AccessDeniedException.php');
include($strRootPath . '/src/controller/api/ControllerInterface.php');
include($strRootPath . '/src/controller/model/DefaultController.php');