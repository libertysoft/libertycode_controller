LibertyCode_Controller
======================



Description
-----------

Library contains access and controller components.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/controller ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/controller": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\controller\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Method access options

---



Usage
-----

#### Controller

Controller allows to design basic controller, 
and control access of itself and its all methods, 
from specified access policy.

_Example_

```php
class ControllerTest extends liberty_code\controller\controller\model\DefaultController
{
    // Define global access policy
    public function checkAccessEngine()
    {
        ...
        // Return boolean
    }
    
    // Define method access policy
    public function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        ...
        // Return boolean
    }
}
...
```

---


