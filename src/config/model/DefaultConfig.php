<?php
/**
 * Description :
 * This class allows to provide configuration for controller features.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\config\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\controller\config\library\ConstConfig;
use liberty_code\controller\config\exception\DefaultMethodInvalidFormatException;



/**
 * @method array getTabDefaultMethodAllowed() Get associative array of default methods allowed. Format: [key: string controller REGEXP => value: string method names[]].
 * @method array getTabDefaultMethodDenied() Get associative array of default methods denied. Format: [key: string controller REGEXP => value: string method names[]].
 * @method void setTabDefaultMethodAllowed(array $tabMethod) Set associative array of default methods allowed. Format: [key: string controller REGEXP => value: string method names[]].
 * @method void setTabDefaultMethodDenied(array $tabMethod) Set associative array of default methods denied. Format: [key: string controller REGEXP => value: string method names[]].
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_METHOD_ALLOWED))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_METHOD_ALLOWED, ConstConfig::DATA_DEFAULT_VALUE_DEFAULT_METHOD_ALLOWED);
		}
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_METHOD_DENIED))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_METHOD_DENIED, ConstConfig::DATA_DEFAULT_VALUE_DEFAULT_METHOD_DENIED);
		}
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstConfig::DATA_KEY_DEFAULT_METHOD_ALLOWED,
			ConstConfig::DATA_KEY_DEFAULT_METHOD_DENIED
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstConfig::DATA_KEY_DEFAULT_METHOD_ALLOWED:
					DefaultMethodInvalidFormatException::setCheck($value);
					break;

				case ConstConfig::DATA_KEY_DEFAULT_METHOD_DENIED:
					DefaultMethodInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
	
	
	// Methods default methods
	// ******************************************************************************
	
	/**
	 * Check if specified controller method exists in allowed methods array.
	 * 
	 * @param string $strCtrlClassPath
	 * @param string $strMethodNm
	 * @return boolean
	 */
	public function checkDefaultMethodAllowedExists($strCtrlClassPath, $strMethodNm)
	{
		// Return result
		return static::checkDefaultMethodExists($this->getTabDefaultMethodAllowed(), $strCtrlClassPath, $strMethodNm);
	}
	
	
	
	/**
	 * Check if specified controller method exists in denied methods array.
	 * 
	 * @param string $strCtrlClassPath
	 * @param string $strMethodNm
	 * @return boolean
	 */
	public function checkDefaultMethodDeniedExists($strCtrlClassPath, $strMethodNm)
	{
		// Return result
		return static::checkDefaultMethodExists($this->getTabDefaultMethodDenied(), $strCtrlClassPath, $strMethodNm);
	}
	
	
	
	/**
	 * Put specified controller method in allowed methods array.
	 * 
	 * @param string $strController
	 * @param string $strMethodNm
	 * @return boolean: true if correctly put, false else
	 */
	public function putDefaultMethodAllowed($strController, $strMethodNm)
	{
		// Init var
		$tabMethod = $this->getTabDefaultMethodAllowed();
		$result = static::putDefaultMethod($tabMethod, $strController, $strMethodNm);
		
		// Register method array
		$this->setTabDefaultMethodAllowed($tabMethod);
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Put specified controller method in denied methods array.
	 * 
	 * @param string $strController
	 * @param string $strMethodNm
	 * @return boolean: true if correctly put, false else
	 */
	public function putDefaultMethodDenied($strController, $strMethodNm)
	{
		// Init var
		$tabMethod = $this->getTabDefaultMethodDenied();
		$result = static::putDefaultMethod($tabMethod, $strController, $strMethodNm);
		
		// Register method array
		$this->setTabDefaultMethodDenied($tabMethod);
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Remove specified controller method (or all methods if no method specified) in allowed methods array.
	 * 
	 * @param string $strController
	 * @param string $strMethodNm = null
	 * @return boolean: true if correctly removed, false else
	 */
	public function removeDefaultMethodAllowed($strController, $strMethodNm = null)
	{
		// Init var
		$tabMethod = $this->getTabDefaultMethodAllowed();
		$result = static::removeDefaultMethod($tabMethod, $strController, $strMethodNm);
		
		// Register method array
		$this->setTabDefaultMethodAllowed($tabMethod);
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Remove specified controller method (or all methods if no method specified) in denied methods array.
	 * 
	 * @param string $strController
	 * @param string $strMethodNm = null
	 * @return boolean: true if correctly removed, false else
	 */
	public function removeDefaultMethodDenied($strController, $strMethodNm = null)
	{
		// Init var
		$tabMethod = $this->getTabDefaultMethodDenied();
		$result = static::removeDefaultMethod($tabMethod, $strController, $strMethodNm);
		
		// Register method array
		$this->setTabDefaultMethodDenied($tabMethod);
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods default methods
	// ******************************************************************************
	
	/**
	 * Check if specified controller method exists in specified methods array.
	 * 
	 * @param array $tabMethod
	 * @param string $strCtrlClassPath
	 * @param string $strMethodNm
	 * @return boolean
	 */
	protected static function checkDefaultMethodExists(array $tabMethod, $strCtrlClassPath, $strMethodNm)
	{
		// Init var
		$result = false;
		
		if(is_string($strCtrlClassPath) && is_string($strMethodNm))
		{
			// Run all methods
			foreach($tabMethod as $strCtrlPattern => $tabMethodNm)
			{
				$result = 
					is_string($strCtrlPattern) && (@preg_match($strCtrlPattern, $strCtrlClassPath) == 1) && // Controller matches
					is_array($tabMethodNm) && in_array($strMethodNm, $tabMethodNm); // Method found
				
				// Break loop if found
				if(!$result)
				{
					break;
				}
			}
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Put specified controller method in specified methods array.
	 * 
	 * @param array &$tabMethod
	 * @param string $strController
	 * @param string $strMethodNm
	 * @return boolean: true if correctly put, false else
	 */
	protected static function putDefaultMethod(array &$tabMethod, $strController, $strMethodNm)
	{
		// Init var
		$result = false;
		
		if(is_string($strController))
		{
			// Init controller methods if required
			if((!isset($tabMethod[$strController])) || (!is_array($tabMethod[$strController])))
			{
				$tabMethod[$strController] = array();
			}
			
			// Register method if required
			if(is_string($strMethodNm) && (!in_array($strMethodNm, $tabMethod[$strController])))
			{
				$tabMethod[$strController][] = $strMethodNm;
				$result = true;
			}
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Remove specified controller method (or all methods if no method specified) in specified methods array.
	 * 
	 * @param array &$tabMethod
	 * @param string $strController
	 * @param string $strMethodNm = null
	 * @return boolean: true if correctly removed, false else
	 */
	protected static function removeDefaultMethod(array &$tabMethod, $strController, $strMethodNm = null)
	{
		// Init var
		$result = false;
		
		if(is_string($strController))
		{
			// Remove all controller methods if required
			if(is_null($strMethodNm))
			{
				$tabMethod[$strController] = array();
				$result = true;
			}
			// Remove specified method if required
			else if(is_string($strMethodNm) && is_array($tabMethod[$strController]) && in_array($strMethodNm, $tabMethod[$strController]))
			{
				// Build new controller array of methods
				$tabCtrlMethodNew = array();
				$tabCtrlMethodOld = $tabMethod[$strController];
				foreach($tabCtrlMethodOld as $strCtrlMethodNm)
				{
					if($strCtrlMethodNm != $strMethodNm)
					{
						$tabCtrlMethodNew[] = $strCtrlMethodNm;
					}
				}
				
				// Register new controller array of methods
				$tabMethod[$strController] = $tabCtrlMethodNew;
				$result = true;
			}
			
			// Build new array of methods
			$tabMethodNew = array();
			foreach($tabMethod as $strCtrlPattern => $tabMethodNm)
			{
				if(is_array($tabMethodNm) && (count($tabMethodNm) > 0))
				{
					$tabMethodNew[$strCtrlPattern] = $tabMethodNm;
				}
			}
			
			// Register new array of methods
			$tabMethod = $tabMethodNew;
		}
		
		// Return result
		return $result;
	}
	
	
	
}