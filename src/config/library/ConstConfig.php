<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_METHOD_ALLOWED = 'tabDefaultMethodAllowed';
	const DATA_KEY_DEFAULT_METHOD_DENIED = 'tabDefaultMethodDenied';
	
    const DATA_DEFAULT_VALUE_DEFAULT_METHOD_ALLOWED = array();
    const DATA_DEFAULT_VALUE_DEFAULT_METHOD_DENIED = array(
		// For all controllers, access methods are denied
		'#.*#' => [
			'checkAccess',
			'checkAccessMethod'
		]
	);
	
	
	
    // Exception message constants
    const EXCEPT_MSG_DEFAULT_METHOD_INVALID_FORMAT = 
		'Following methods "%1$s" invalid! ' . 
		'The methods must be an array with key as string not empty (REGEXP of controller), ' . 
		'and value as index array of strings not empty (method names).';
}