<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\config\exception;

use liberty_code\controller\config\library\ConstConfig;



class DefaultMethodInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $method
     */
	public function __construct($method)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
            ConstConfig::EXCEPT_MSG_DEFAULT_METHOD_INVALID_FORMAT,
            mb_strimwidth(strval($method), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if method has valid format.
	 * 
     * @param mixed $method
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($method)
    {
		// Init var
		$result = is_array($method); // Check is valid array
		
		if($result)
		{
			// Run all methods
			foreach($method as $strCtrlPattern => $tabMethodNm)
			{
				// Check controller info
				$result = 
					is_string($strCtrlPattern) && (trim($strCtrlPattern) !== '') && // Check controller REGEXP is valid string
					is_array($tabMethodNm) && (count($tabMethodNm) > 0); // Check methods are valid array
				
				// Run all controller methods if required
				if($result)
				{
					for($intCpt = 0; $result && ($intCpt < count($tabMethodNm)); $intCpt++)
					{
						// Check method name is valid string
						$result = (is_string($tabMethodNm[$intCpt]) && (trim($tabMethodNm[$intCpt]) !== ''));
					}
				}
				
				// Break loop if check not passed
				if(!$result)
				{
					break;
				}
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($method) ? serialize($method) : $method));
		}
		
		// Return result
		return $result;
    }
	
	
	
}