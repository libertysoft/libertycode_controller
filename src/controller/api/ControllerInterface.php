<?php
/**
 * Description :
 * This class allows to describe behavior of controller class.
 * Controller is item, allows to control access of itself and its all methods from a specified access policy.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\api;



interface ControllerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if right to access to this class.
	 * 
	 * @return boolean
	 */
	public function checkAccess();
	
	
	
	/**
	 * Check if right to access to the specified method with specified arguments if required.
	 * 
	 * @param string $strMethodNm
	 * @param array $tabArg = null
	 * @return boolean
	 */
	public function checkAccessMethod($strMethodNm, array $tabArg = null);
}