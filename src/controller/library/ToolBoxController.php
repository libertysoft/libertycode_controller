<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\controller\config\model\DefaultConfig;
use liberty_code\controller\controller\api\ControllerInterface;
use liberty_code\controller\controller\exception\AccessDeniedException;



class ToolBoxController extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check by default, if right to access to the specified controller method with specified arguments if required.
	 * 
	 * @param ControllerInterface $objController
	 * @param string $strMethodNm
	 * @param array $tabArg = null
	 * @return boolean
	 */
	public static function checkDefaultAccessMethod(ControllerInterface $objController, $strMethodNm, array $tabArg = null)
	{
		// Init var
		$result = false;
		$objClass = new \ReflectionClass($objController);
		
		// Check method found
		if($objClass->hasMethod($strMethodNm))
		{
			// Get info
			/** @var DefaultConfig $objConfig */
			$objConfig = DefaultConfig::instanceGetDefault();
			$objMethod = $objClass->getMethod($strMethodNm);
			
			// Check access
			$result = 
			$objMethod->isPublic() && // Check method is public
			(
				$objConfig->checkDefaultMethodAllowedExists($objClass->getName(), $strMethodNm) || // Method automatically allowed
				(!$objConfig->checkDefaultMethodDeniedExists($objClass->getName(), $strMethodNm)) // Method automatically denied
			) && 
			// Check arguments valid if required
			(
				is_null($tabArg) || 
				ToolBoxReflection::checkFunctionArgValid($objMethod, $tabArg)
			);
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
     * Check if right to access to the specified controller.
     *
     * @param ControllerInterface $objController
	 * @param string $strMethodNm = null
	 * @param array $tabArg = null
	 * @return boolean
     */
    public static function checkAccess(ControllerInterface $objController, $strMethodNm = null, array $tabArg = null)
    {
        // Init var
		$result = $objController->checkAccess();
        
		if(
			$result && // Check right access to the controller
			(!is_null($strMethodNm)) && is_string($strMethodNm) && (trim($strMethodNm) != '') // Check method access required
		)
		{
			// Check right access to the method
			$result = $objController->checkAccessMethod($strMethodNm, $tabArg);
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Set access check for specified controller.
     *
     * @param ControllerInterface $objController
	 * @param string $strMethodNm = null
	 * @param array $tabArg = null
	 * @throws AccessDeniedException
     */
    public static function setCheckAccess(ControllerInterface $objController, $strMethodNm = null, array $tabArg = null)
    {
        // Init var
		$result = static::checkAccess($objController, $strMethodNm, $tabArg);

		// Throw exception if check not pass
		if(!$result)
		{
			$objClass = new \ReflectionClass($objController);
			throw new AccessDeniedException($objClass->getName());
		}
    }
	
	
	
}