<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\library;



class ConstController
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration
	const CONF_DEFAULT_CACHE_ACCESS_OPTION = true;
	
	
	
    // Exception message constants
	const EXCEPT_MSG_ACCESS_DENIED = 'Access denied for following class "%1s"!';
    const EXCEPT_MSG_DEFAULT_CACHE_ACCESS_OPTION_INVALID_FORMAT = 'Following option "%1$s" invalid! The option must be a boolean.';
}