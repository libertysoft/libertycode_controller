<?php

namespace liberty_code\controller\controller\test;

use liberty_code\controller\controller\model\DefaultController;



class ControllerTest extends DefaultController
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkAccessEngine()
	{
		// Init var
		$result = parent::checkAccessEngine() && isset($_GET['test']);
		echo('<br />Access calcul<br />');
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
	{
		// Init var
		$result = 
			parent::checkAccessMethodEngine($strMethodNm, $tabArg) && 
			(isset($_GET['action3']) || ($strMethodNm != 'action3'));
		echo('<br />Access method calcul<br />');
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods action
    // ******************************************************************************
	
    public function action1($strAdd)
    {
        // Return result
        return 'controller test: action 1' . 
		((trim($strAdd) != '') ? ': ' . $strAdd : '');
    }
	
	
	
	protected function action2()
    {
        // Return result
        return 'controller test: action 2';
    }
	
	
	
	public function action3($strAdd, $strAdd2 = '')
    {
        // Return result
        return 'controller test: action 3' . 
		((trim($strAdd) != '') ? ': ' . $strAdd : ''). 
		((trim($strAdd2) != '') ? ': ' . $strAdd2 : '');
    }
	
	
	
}