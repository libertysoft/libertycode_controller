<?php

/**
 * Test http:
 * -> Test access controller OK: GET ?test=1
 * -> Test access controller method "action3" OK: GET ?action3=1
 * -> Test access controller and controller method "action3" OK: GET ?test=1&action3=1
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/controller/test/ControllerTest.php');

// Use
use liberty_code\controller\config\model\DefaultConfig;
use liberty_code\controller\controller\test\ControllerTest;
use liberty_code\controller\controller\library\ToolBoxController;



// Init var
/** @var DefaultConfig $objConfig */
$objConfig = DefaultConfig::instanceGetDefault();
$objControllerTest = new ControllerTest();



// Test configuration
echo('Test configuration: <br />');

echo('Get allowed methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodAllowed());echo('</pre>');
echo('Get denied methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodDenied());echo('</pre>');

$objConfig->setTabDefaultMethodAllowed(array());
$objConfig->setTabDefaultMethodDenied(array());
echo('Get allowed methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodAllowed());echo('</pre>');
echo('Get denied methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodDenied());echo('</pre>');

try{
	$objConfig->setTabDefaultMethodAllowed('test');
} catch(\Exception $e) {
	echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
	echo('<br />');
}

try{
	$objConfig->setTabDefaultMethodDenied('test');
} catch(\Exception $e) {
	echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
	echo('<br />');
}

$objConfig->putDefaultMethodAllowed('#.*#', 'test_1');
$objConfig->putDefaultMethodAllowed('#.*#', 'test_2');
$objConfig->putDefaultMethodAllowed('#.*#', 'test_3');
$objConfig->putDefaultMethodAllowed('#test1#', 'test_1');
$objConfig->putDefaultMethodAllowed('#test1#', 'test_2');
$objConfig->putDefaultMethodAllowed('#test1#', 'test_3');

$objConfig->putDefaultMethodDenied('#.*#', 'checkAccess');
$objConfig->putDefaultMethodDenied('#.*#', 'checkAccessMethod');
$objConfig->putDefaultMethodDenied('#.*#', 'test_1');
$objConfig->putDefaultMethodDenied('#test1#', 'test_1');
$objConfig->putDefaultMethodDenied('#test1#', 'test_2');
$objConfig->putDefaultMethodDenied('#test1#', 'test_3');

echo('Get allowed methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodAllowed());echo('</pre>');
echo('Get denied methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodDenied());echo('</pre>');

$objConfig->removeDefaultMethodAllowed('#.*#', 'test_2');
$objConfig->removeDefaultMethodAllowed('#.*#', 'test_3');
$objConfig->removeDefaultMethodAllowed('#test1#');

$objConfig->removeDefaultMethodDenied('#.*#', 'test_1');
$objConfig->removeDefaultMethodDenied('#test1#');

echo('Get allowed methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodAllowed());echo('</pre>');
echo('Get denied methods: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodDenied());echo('</pre>');

$objConfig->removeDefaultMethodAllowed('#.*#');
//$objConfig->removeDefaultMethodDenied('#.*#');
echo('Get allowed methods final: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodAllowed());echo('</pre>');
echo('Get denied methods final: <pre>');var_dump(ControllerTest::getObjConfig()->getTabDefaultMethodDenied());echo('</pre>');

echo('<br /><br /><br />');



// Test access
$tabMethod = array(
	// Access ko: Method denied
	['checkAccess', null],
	// Access ko: Method denied
	['checkAccessMethod', ['test_value_1', []]],
	// Access ok
	['action1', null],
	// Access ok
	['action1', ['test value 1']],
	// Access ko: Method arguments invalid
	['action1', ['test value 1', 'test value 2']],
	// Access ko: Method not public
	['action2', ['test value 1', 'test value 2']],
	// Access ok
	['action3', null],
	// Access ok
	['action3', ['test value 1']],
	// Access ok
	['action3', ['test value 1', 'test value 2']],
	// Access ko: Method arguments invalid
	['action3', ['test value 1', 'test value 2', 'test value 3']],
	// Access ko: Method not found
	['action4', ['test value 1', 'test value 2']]
);

foreach($tabMethod as $tabMethodInfo)
{
	// Get info
	$strMethodNm = $tabMethodInfo[0];
	$tabArg = $tabMethodInfo[1];
	
	$objControllerTest->cacheAccessInitData();
	
	// Print test
	echo('Test access "'.$strMethodNm.'": <br />');
	echo('Arguments: ');var_dump($tabArg);echo('<br />');
	
	echo('Check access main: <pre>');var_dump($objControllerTest->checkAccess());echo('</pre>');
	echo('Check access main 2: <pre>');var_dump($objControllerTest->checkAccess());echo('</pre>');
	echo('Check access method: <pre>');var_dump($objControllerTest->checkAccessMethod($strMethodNm, $tabArg));echo('</pre>');
	echo('Check access method 2: <pre>');var_dump($objControllerTest->checkAccessMethod($strMethodNm, $tabArg));echo('</pre>');
	
	echo('Check default access method (by toolbox): <pre>');var_dump(ToolBoxController::checkDefaultAccessMethod($objControllerTest, $strMethodNm, $tabArg));echo('</pre>');
	
	echo('Check access main (by toolbox): <pre>');var_dump(ToolBoxController::checkAccess($objControllerTest));echo('</pre>');
	echo('Check access main (by toolbox) 2: <pre>');var_dump(ToolBoxController::checkAccess($objControllerTest));echo('</pre>');
	echo('Check access main and method (by toolbox): <pre>');var_dump(ToolBoxController::checkAccess($objControllerTest, $strMethodNm, $tabArg));echo('</pre>');
	echo('Check access main and method (by toolbox) 2: <pre>');var_dump(ToolBoxController::checkAccess($objControllerTest, $strMethodNm, $tabArg));echo('</pre>');
	
	// Test exception
	try{
		ToolBoxController::setCheckAccess($objControllerTest);
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
		echo('<br />');
	}
	
	try{
		ToolBoxController::setCheckAccess($objControllerTest, $strMethodNm, $tabArg);
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


