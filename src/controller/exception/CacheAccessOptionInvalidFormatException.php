<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\exception;

use liberty_code\controller\controller\library\ConstController;



class CacheAccessOptionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $option
     */
	public function __construct($option)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstController::EXCEPT_MSG_DEFAULT_CACHE_ACCESS_OPTION_INVALID_FORMAT, strval($option));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified option has valid format.
	 * 
     * @param mixed $option
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($option)
    {
		// Init var
		$result = 
			is_bool($option) || 
			is_int($option) ||
			(is_string($option) && ctype_digit($option));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($option);
		}
		
		// Return result
		return $result;
    }
	
	
	
}