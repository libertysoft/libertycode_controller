<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\exception;

use liberty_code\controller\controller\library\ConstController;



class AccessDeniedException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $class
     */
	public function __construct($class)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstController::EXCEPT_MSG_ACCESS_DENIED, strval($class));
	}
	
	
	
}