<?php
/**
 * Description :
 * This class allows to define default controller class.
 * Can be consider is base of all controller type.
 * Access caching system (activated by default) is set for right access features.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\controller\controller\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\controller\controller\api\ControllerInterface;

use liberty_code\controller\config\model\DefaultConfig;
use liberty_code\controller\controller\library\ConstController;
use liberty_code\controller\controller\library\ToolBoxController;
use liberty_code\controller\controller\exception\CacheAccessOptionInvalidFormatException;



abstract class DefaultController extends DefaultBean implements ControllerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/**
	 * Allowed caching for right access data.
     * @var boolean
     */
	protected $__cacheAccessBoolOption;
	
	
	
	/**
	 * Caching data about right access to this class.
     * @var boolean
     */
	protected $__cacheAccessBool;
	
	
	
	/**
	 * Caching data about right access to methods.
     * @var array
     */
	protected $__cacheAccessTabMethod;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @param boolean $cacheAccessBoolOption = ConstController::CONF_DEFAULT_CACHE_ACCESS_OPTION
     */
	public function __construct($cacheAccessBoolOption = ConstController::CONF_DEFAULT_CACHE_ACCESS_OPTION, $tabData = array()) 
	{
		// Call parent constructor
		parent::__construct($tabData);
		
		// Init access caching
		$this->cacheAccessSetOption($cacheAccessBoolOption);
		$this->cacheAccessInitData();
	}
	
	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if right to access to this class.
	 * 
	 * @return boolean
	 */
	protected function checkAccessEngine()
	{
		// Return result
		return true;
	}
	
	
	
	/**
	 * Wrapping method used cache if required, to return result.
	 * @inheritdoc
	 */
	public function checkAccess()
	{
		// Init var
		$result = false;
		
		// Used cache
		if($this->cacheAccessCheckOption())
		{
			// Init access data, if required
			if(is_null($this->__cacheAccessBool) || (!is_bool($this->__cacheAccessBool)))
			{
				$this->__cacheAccessBool = $this->checkAccessEngine();
			}
			
			$result = $this->__cacheAccessBool;
		}
		// Dont used cache
		else
		{
			$result = $this->checkAccessEngine();
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Check if right to access to the specified method with specified arguments if required.
	 * 
	 * @param string $strMethodNm
	 * @param array $tabArg = null
	 * @return boolean
	 */
	protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
	{
		// Return result
		return ToolBoxController::checkDefaultAccessMethod($this, $strMethodNm, $tabArg);
	}
	
	
	
	/**
	 * Wrapping method used cache if required, to return result.
	 * @inheritdoc
	 */
	public function checkAccessMethod($strMethodNm, array $tabArg = null)
	{
		// Init var
		$result = false;
		
		// Used cache
		if($this->cacheAccessCheckOption())
		{
			// Get info
			$strCacheKey = static::getStrHashMethodArg($strMethodNm, $tabArg);
			
			// Init access data, if required
			if((!isset($this->__cacheAccessTabMethod[$strCacheKey])) || (!is_bool($this->__cacheAccessTabMethod[$strCacheKey])))
			{
				$this->__cacheAccessTabMethod[$strCacheKey] = $this->checkAccessMethodEngine($strMethodNm, $tabArg);
			}
			
			$result = $this->__cacheAccessTabMethod[$strCacheKey];
		}
		// Dont used cache
		else
		{
			$result = $this->checkAccessMethodEngine($strMethodNm, $tabArg);
		}
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods access caching
	// ******************************************************************************
	
	/**
	 * Initialize cache access data.
	 */
	public function cacheAccessInitData()
	{
		// Init var
		$this->__cacheAccessBool = null;
		$this->__cacheAccessTabMethod = array();
	}
	
	
	
	/**
	 * Check caching for right access data, is allowed.
	 *
	 * @return boolean
	 */
	public function cacheAccessCheckOption()
	{
		// Return result
		return $this->__cacheAccessBoolOption;
	}
	
	
	
	/**
	 * Allowed caching for right access data.
	 *
	 * @param boolean $boolOption = ConstController::CONF_DEFAULT_CACHE_ACCESS_OPTION
	 * @throws CacheAccessOptionInvalidFormatException
	 */
	public function cacheAccessSetOption($boolOption = ConstController::CONF_DEFAULT_CACHE_ACCESS_OPTION)
	{
		// Set check option
		CacheAccessOptionInvalidFormatException::setCheck($boolOption);
		
		// Init var
		$resultOption = $boolOption; // Case option is boolean
		
		// Case option is like boolean
		if(
			is_int($boolOption) ||
			(is_string($boolOption) && ctype_digit($boolOption))
		)
		{
			$resultOption = (intval($boolOption) != 0);
		}
		
		// Register option
		$this->__cacheAccessBoolOption = $resultOption;
	}
	
	
	
	
	
	// Methods statics
    // ******************************************************************************

    /**
     * Get controller configuration.
     *
     * @return DefaultConfig
     */
    public static function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }
	
	
	
	/**
	 * Get string hash for specified method and specified arguments, if required.
	 * 
	 * @param string $strMethodNm
	 * @param array $tabArg = null
	 * @return null|string
	 */
    protected static function getStrHashMethodArg($strMethodNm, array $tabArg = null)
    {
		// Init var
		$result = null;
		
		if(is_string($strMethodNm))
		{
			$result = md5($strMethodNm . ((!is_null($tabArg)) ? serialize($tabArg) : ''));
		}
		
        // Return result
        return $result;
    }
	
	
	
}